# backpack.tf automatic #

*Read this carefully before doing anything, it contains a full set of instructions. Read past the download links before asking for help.*

### What is this for? What does it do? ###

* Automatically accept incoming offers that match your listings on [backpack.tf](http://backpack.tf).
* Supports multiple listings in a single offer.
* Automatically rejects any incoming offer from a banned user or tagged scammer.
* Please note that at this moment, it only supports TF2 offers.
* The bot will handle change as part of the trade offer in metal only. If you're asking for 1 key, it will not accept anything else than 1 key. If you're asking 8.33 refined for an item and the buyer offers 9 refined while asking the item + 2 reclaimed metal back, it will process the offer normally.
* 'Ghost' trade offers, where items are unavailable, are automatically declined. Normally it's not possible to decline them manually.
* Every 5 minutes an heartbeat is dispatched to the backpack.tf server. This allows us to identify which users are currently running the bot and switching the trade offer button to an automatic trade offer button.

### How do I get set up? ###

#### Easy install - Windows, Mac and Linux####
* Install [Node.js](https://nodejs.org/download/) - 32 bit version for 32 bit systems or 64 bit version for 64 bit systems (0.12 minimum)
* [Download latest version](https://bitbucket.org/srabouin/backpack.tf-automatic/downloads) and *unpack it to a folder of your choice*, it will not work if you do not unpack it.
* Windows users:
    * Double-click automatic.bat.
        * If you get an error and you've completed everything above, please try to restart your computer. Sometimes a reboot is necessary for node to function properly on certain systems.
* Fill in the details. See the [Running the application](#running-the-application) section below.

#### Power Users ####
* Assuming you have nodejs, npm and git installed and at least basic knowledge of command line.
* Run the following commands:
```sh
git clone https://bitbucket.org/srabouin/backpack.tf-automatic.git
cd backpack.tf-automatic
npm install
node bot
```
* Optionally, if you don't want to be bothered with crashes, you can use `forever` or a similar package. On the command line, install it with `npm install -g forever`. Then you can use it as a node replacement like such: `forever automatic.js`. Don't forget `.js`, it is mandatory with forever.

### Running the application ###

* The bot will ask you for your Steam details and your backpack.tf token. You can find your token on your [Settings](http://backpack.tf/my/preferences) page, in the "Advanced" section.
* You can get your Steam Guard code from your email or app (only valid once). If you have family view enabled, you will also have to give your PIN.
* *Your password will be hidden during setup, it will still accept keystrokes. Use enter to submit as usual.*
* Place the items you are selling at the beginning of your backpack so they are easier to find, especially if you have multiple identical items and only selling one. backpack.tf relies on the item id, so if the person sending you a trade offer picks the wrong item, the offer will not be automatically accepted as it will not be able to match your item. By placing it at the beginning of your backpack and then creating a listing for your item, you will ensure the proper item is easily accessible.

### Console Commands ###

There are a few console commands that you can type into the bot's console window.

- `help` - Shows the list of all valid console commands
- `identity_secret` - If you have the Steam Guard Mobile Authenticator enabled and you've extracted your keys from your phone (via rooting or jailbreaking), you can use `identity_secret <your identity_secret key>` and Automatic will accept all trade confirmations automatically for you.
    - **IMPORTANT:** This will accept **all** trade confirmations, not just ones for offers that Automatic has accepted!
- `logout` - Log out of Steam and forget your saved logon, if you've saved it. Shuts down Automatic.

### Settings (Advanced) ###

Automatic has some optional configuration to it, which you can change by editing the settings.json file. The important settings are listed in this section.

- acceptOverpay: Change the value from `false` to `true` to automatically accept currency overpay. (for example, you asked for 2 ref and you were offered 3 ref, it will automatically accept)
- acceptGifts: Change the value from `false` to `true` to accept any items alongside currency, assuming your wanted amount of currency is matched. Normally you would have to accept such an offer manually. Offers which do not have items on one side are not automatically accepted. Note that offers where there are no items on either side are not accepted.
- acceptedKeys: Add strings to this array of item market names to signal you wish to accept such keys as key currency. For example, the `End of the Line Key`. As very advanced usage you can even include other items that you consider to be a key.

### I get a specific error when I start the bot, what does it mean? ###
#### 'node' is not recognized as an internal or external command, operable program or batch file ####
Restart your computer.

#### %1 is not a valid Win32 application.
You need to install the proper versions for your Windows bitness (32 or 64 bit). So if you have a 32 bit Windows install, everything has to be 32 bit. If it's 64 bit, everything has to be 64 bit.

#### Cannot find module '...' (commonly 'C:\Windows\system32\bot')
Unzip the bot somewhere you like (such as your desktop folder).

### Who do I talk to if I run into problems, want to report a bug, or want to suggest features? ###

* Please use the [issues](https://bitbucket.org/srabouin/backpack.tf-automatic/issues?status=new&status=open) section of this repo for bug reports and feature suggestions.
* Ask the community for help on the [backpack.tf forums](http://forums.backpack.tf/index.php?/topic/20204-backpacktf-automatic-help-thread/).