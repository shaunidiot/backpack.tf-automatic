var TradeOfferManager = require('steam-tradeoffer-manager');
var Prompt = require('prompt');
var request = require('request');
var Config = require('./config.js');
var log = require('./logging.js');
var steam = require('./steamclient.js');
var manager = require('./trade.js');

const BASE_URL = (Config.get().backpackDomain || 'https://backpack.tf') + '/api/';

exports.heartbeat = heartbeat;
exports.handleOffer = handleOffer;

var g_OfferDetails = {};

function getSavedToken() {
	var config = Config.get();
	var sid = steam.steamID.getSteamID64();

	if(!config.tokens || !config.tokens[sid]) {
		return null;
	}

	return config.tokens[sid];
}

function heartbeat() {
	var token = getSavedToken();
	if(!token) {
		getToken(heartbeat);
		return;
	}

	request.post({
		"uri": BASE_URL + "IAutomatic/IHeartBeat/",
		"form": {
			"method": "alive",
			"version": require('../package.json').version,
			"steamid": steam.steamID.toString(),
			"token": getSavedToken()
		},
		"json": true
	}, function(err, response, body) {
		if(err || response.statusCode != 200) {
			log.warn("Error occurred contacting backpack.tf (" + (err ? err.message : "HTTP error " + response.statusCode) + "), trying again in 60 seconds");
			setTimeout(heartbeat, 60000);
		} else if(!body.success) {
			log.warn("Invalid backpack.tf token");
			getToken(heartbeat);
		} else {
			log.verbose("Heartbeat sent to backpack.tf." + (body.bumped ? " " + body.bumped + " listing" + (body.bumped == 1 ? '' : 's') + " bumped." : ""));
			setTimeout(heartbeat, 60000 * 5); // 5 minutes
			global.heartbeat = true;
		}
	});
}

function handleOffer(offer, currencies) {
	// TODO: Buy orders
	request.get({
		"uri": BASE_URL + "IGetUserTrades/v1/",
		"qs": {
			"steamid": steam.steamID.toString(),
			"steamid_other": offer.partner.toString(),
			"ids": offer.itemsToGive.map(function(item) {
				return item.assetid || item.id;
			})
		},
		"json": true
	}, function(err, response, body) {
		if (err || response.statusCode != 200 || !body.response || !body.response.success) {
			log.warn("Error occurred getting trade list (" + (err ? err.message : "HTTP error " + response.statusCode) + "), trying again in 60 seconds");
			setTimeout(function () {
				handleOffer(offer);
			}, 60000);
		}

		if (body.response.other && (body.response.other.scammer || body.response.other.banned)) {
			var decline = Config.get().declineBanned;
			log.info(offer.partner.getSteam3RenderedID() + " Sender of offer #" + offer.id + " is marked as a scammer or banned" + (decline ? ", declining" : ""));

			if (decline) {
				offer.decline();
			}

			return;
		}

		var listings = body.response.store;
		if (listings.length === 0) {
			log.info(offer.partner.getSteam3RenderedID() + " No matching listings found for offer #" + offer.id + ", skipping");
			return;
		}

		// Check if the offer would make us lose any items that aren't in a listing
		var i, item, isChange;
		for (i = 0; i < offer.itemsToGive.length; i++) {
			item = offer.itemsToGive[i];
			isChange = false;

			if (item.appid != 440) {
				log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " contains non-TF2 items, skipping");
				return;
			}

			if (item.app_data && item.app_data.def_index) {
				switch (parseInt(item.app_data.def_index, 10)) {
					case 5000:
						isChange = true;
						currencies.metal -= 1/9;
						break;

					case 5001:
						isChange = true;
						currencies.metal -= 1/3;
						break;

					case 5002:
						isChange = true;
						currencies.metal--;
						break;
				}
			}

			if (!isChange) {
				// Check if it's in any listing
				if (!listings.some(function (listing) {
							return listing.item && listing.item.id == (item.assetid || item.id);
						})) {
					log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " contains an item that isn't in a listing (" + item.name + "), skipping");
					return;
				}
			}
		}

		var requiredCurrencies = {
			"metal": 0,
			"keys": 0
		};

		listings.forEach(function (listing) {
			for (var i in listing.currencies) {
				if (!listing.currencies.hasOwnProperty(i)) {
					continue;
				}

				requiredCurrencies[i] += listing.currencies[i];
			}
		});

		var config = Config.get();
		for (i in requiredCurrencies) {
			if (!requiredCurrencies.hasOwnProperty(i)) {
				continue;
			}

			// Truncate everything past the second decimal place
			requiredCurrencies[i] = Math.floor(requiredCurrencies[i] * 100) / 100;

			if (requiredCurrencies[i] === 0) {
				// We don't need any of this currency
				continue;
			}

			if (currencies[i]) {
				currencies[i] = Math.floor(currencies[i] * 100) / 100;
			}

			if (!currencies[i] || currencies[i] < requiredCurrencies[i]) {
				log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " doesn't offer enough " + i + " (offered = " + currencies[i] + ", required = " + requiredCurrencies[i] + "), skipping");
				return;
			}

			if (!config.acceptOverpay && currencies[i] > requiredCurrencies[i]) {
				log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " offered too much " + i + " (offered = " + currencies[i] + ", required = " + requiredCurrencies[i] + "), skipping");
				return;
			}
		}

		// Make sure escrow is okay
		canEscrow(offer, function (canAccept) {
			if(!canAccept) {
				return;
			}

			// Everything looks good
			log.debug(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + ": Required = " + JSON.stringify(requiredCurrencies) + ", offered = " + JSON.stringify(currencies));
			log.trade(offer.partner.getSteam3RenderedID() + " Everything in offer #" + offer.id + " looks good, accepting");

			var message = "Asked: ";

			for (i in requiredCurrencies) {
				if (!requiredCurrencies.hasOwnProperty(i) || !requiredCurrencies[i]) {
					continue;
				}

				message += requiredCurrencies[i] + " " + i + " ";
			}

			message += "(" + itemSummary(offer.itemsToGive) + "). Offered: ";

			for (i in currencies) {
				if (!currencies.hasOwnProperty(i) || !currencies[i]) {
					continue;
				}

				message += currencies[i] + " " + i + " ";
			}

			message += "(" + itemSummary(offer.itemsToReceive) + ").";

			log.trade(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " - " + message);

			g_OfferDetails[offer.id] = message;

			offer.accept(function (err, status) {
				if (err) {
					var msg = err.message;

					if (err.cause) {
						msg = err.cause;
					} else if (err.eresult) {
						msg = TradeOfferManager.EResult.getName(err.eresult);
					}

					log.warn(offer.partner.getSteam3RenderedID() + " Unable to accept offer #" + offer.id + ": " + msg);
				} else {
					log.trade(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " successfully accepted" + (status == 'pending' ? "; confirmation required" : ""));
					notifyOfferAccepted(offer, message);
				}
			});
		});
	});
}

function itemSummary(items) {
	var names = {};

	items.forEach(function(item) {
		if(item.market_hash_name in names) {
			names[item.market_hash_name]++;
		} else {
			names[item.market_hash_name] = 1;
		}
	});

	var formattedNames = [];
	for(var name in names) {
		if(!names.hasOwnProperty(name)) {
			continue;
		}

		formattedNames.push(name + (names[name] > 1 ? " x" + names[name] : ""));
	}

	return formattedNames.join(', ');
}

function canEscrow(offer, callback) {
	if(Config.get().acceptEscrow) {
		// User doesn't care about escrow
		callback(true);
		return;
	}

	offer.getEscrowDuration(function(err, theirDays, myDays) {
		if(err) {
			log.error("Cannot check escrow duration for offer #" + offer.id + ": " + err.message);
			callback(false);
			return;
		}

		var escrowDays = 0;
		if(offer.itemsToReceive.length > 0 && theirDays > escrowDays) {
			escrowDays = theirDays;
		}

		if(offer.itemsToGive.length > 0 && myDays > escrowDays) {
			escrowDays = myDays;
		}

		if(escrowDays > 0) {
			log.warn("Offer #" + offer.id + " would incur up to " + escrowDays + " escrow. Not accepting.");
			callback(false);
		} else {
			callback(true);
		}
	});
}

function notifyOfferAccepted(offer, message) {
	request.post({
		"uri": BASE_URL + "IAutomatic/IOfferDetails/",
		"form": {
			"method": "completed",
			"steamid": offer.manager.steamID.getSteamID64(),
			"version": require('../package.json').version,
			"token": getSavedToken(),
			"message": message,
			"offer": {
				"tradeofferid": offer.id,
				"accountid_other": offer.partner.accountid,
				"steamid_other": offer.partner.getSteamID64(),
				"message": offer.message,
				"expiration_time": Math.floor(offer.expires.getTime() / 1000),
				"trade_offer_state": offer.state,
				"is_our_offer": offer.isOurOffer ? "true" : "false",
				"time_created": Math.floor(offer.created.getTime() / 1000),
				"time_updated": Math.floor(offer.updated.getTime() / 1000),
				"from_real_time_trade": offer.fromRealTimeTrade ? "true" : "false",
				"items_to_give": offer.itemsToGive.map(extractAssetInfo),
				"items_to_receive": offer.itemsToReceive.map(extractAssetInfo),
				"confirmation_method": offer.confirmationMethod || 0,
				"escrow_end_date": offer.escrowEnds ? Math.floor(offer.escrowEnds.getTime() / 1000) : 0
			}
		}
	}, function(err, response, body) {
		if(err || response.statusCode != 200) {
			log.warn("Cannot contact backpack.tf, retrying... - " + (err ? err.message : "HTTP error " + response.statusCode));
			setTimeout(function() {
				notifyOfferAccepted(offer, message);
			}, 30000);
		}
	});

	function extractAssetInfo(item) {
		return {
			"appid": item.appid,
			"contextid": item.contextid,
			"assetid": item.assetid || item.id,
			"classid": item.classid,
			"instanceid": item.instanceid || "0",
			"amount": item.amount || "1",
			"missing": item.missing ? "true" : "false"
		};
	}
}

manager.on('receivedOfferChanged', function(offer, oldState) {
	if(offer.state == TradeOfferManager.ETradeOfferState.Accepted && g_OfferDetails[offer.id]) {
		// Send notification
		var offerDetails = {
			"tradeofferid": offer.id,
			"accountid_other": offer.partner.accountid,
			"steamid_other": offer.partner.getSteamID64(),
			"message": offer.message,
			"expiration_time": Math.floor(offer.expires.getTime() / 1000),
			"trade_offer_state": offer.state,
			"is_our_offer": offer.isOurOffer,
			"time_created": Math.floor(offer.created.getTime() / 1000),
			"time_updated": Math.floor(offer.updated.getTime() / 1000),
			"from_real_time_trade": offer.fromRealTimeTrade
		};

		if(offer.itemsToGive.length > 0) {
			offerDetails.items_to_give = offer.itemsToGive.map(function(item) {
				return {
					"appid": item.appid,
					"contextid": item.contextid,
					"assetid": item.assetid,
					"classid": item.classid,
					"instanceid": item.instanceid,
					"amount": item.amount,
					"missing": !!item.missing
				};
			});
		}

		if(offer.itemsToReceive.length > 0) {
			offerDetails.items_to_receive = offer.itemsToReceive.map(function(item) {
				return {
					"appid": item.appid,
					"contextid": item.contextid,
					"assetid": item.assetid,
					"classid": item.classid,
					"instanceid": item.instanceid,
					"amount": item.amount,
					"missing": !!item.missing
				};
			});
		}

		sendRequest();

		function sendRequest() {
			request.post({
				"uri": BASE_URL + "IAutomatic/IOfferDetails/",
				"form": {
					"method": "completed",
					"steamid": steam.steamID.getSteamID64(),
					"version": require('../package.json').version,
					"token": getSavedToken(),
					"offer": offerDetails,
					"message": g_OfferDetails[offer.id]
				}
			}, function (err, response) {
				if (err || response.statusCode != 200) {
					log.warn("[" + offer.id + "] Error reaching backpack.tf, resending in 60 seconds...");
					setTimeout(sendRequest, 60000);
				} else {
					log.debug("Notification sent to backpack.tf for offer #" + offer.id);
				}
			});
		}
	}
});

function getToken(callback) {
	Prompt.get({
		"properties": {
			"token": {
				"description": "backpack.tf token".green,
				"type": "string",
				"required": true
			}
		}
	}, function(err, result) {
		if(err) {
			log.error("Cannot read backpack.tf token: " + err.message);
			process.exit(1);
			return;
		}

		var config = Config.get();
		config.tokens = config.tokens || {};
		config.tokens[steam.steamID.getSteamID64()] = result.token;
		Config.write(config);

		if(callback) {
			callback();
		}
	});
}
