var SteamCommunity = require('steamcommunity');
var Prompt = require('prompt');
var open = require('open');
var request = require('request');
var Config = require('./config.js');
var log = require('./logging.js');

var client = new SteamCommunity();
client.username = null;

module.exports = client;

// Set up prompt
Prompt.message = Prompt.delimiter = "";

var g_Cookies = [];
var g_RelogInterval = null;

setTimeout(function() {
	var config = Config.get();

	var username = config.steam ? config.steam.last : null;

	if(username && config.steam && config.steam.sentries && config.steam.sentries[username] && config.steam.oAuthTokens && config.steam.oAuthTokens[username]) {
		log.info("Logging into Steam with oAuth token");

		client.oAuthLogin(config.steam.sentries[username], config.steam.oAuthTokens[username], function(err, sessionID, cookies) {
			if(err) {
				log.error("Cannot login to Steam: " + err.message);
				promptLogin();
				return;
			}

			log.info("Logged into Steam!");
			client.setCookies(cookies);
			g_Cookies = cookies;

			checkLogin();
		});
	} else {
		promptLogin();
	}
}, 50);

function checkLogin() {
	client.loggedIn(function(err, loggedIn, familyView) {
		if (err) {
			log.error("Cannot check Steam login: " + err);
			setTimeout(checkLogin, 5000);
		} else if (!loggedIn) {
			log.warn("Saved login cookies are no longer valid.");
			promptLogin();
		} else {
			if (familyView) {
				log.warn("This account is protected by Family View.");
				promptFamilyView();
			} else {
				require('./backpacktf.js').heartbeat();
				var interval = setInterval(function() {
					if(!global.heartbeat) {
						// We aren't online on backpack.tf yet
						return;
					}

					log.verbose("Setting Trade Offer Manager cookies.");
					require('./trade.js').setCookies(g_Cookies, function(err) {
						if(err) {
							log.error("Cannot get API key: " + err.message);
							process.exit(1);
							return;
						}

						log.info("Trade manager ready to go.");
						checkOfferCount();

						// Start the input console
						require('./console.js');
					});

					clearInterval(interval);

					if(!g_RelogInterval) {
						g_RelogInterval = setInterval(relog, 1000 * 60 * 60 * 2); // every 2 hours
					}
				}, 1000);
			}
		}
	})
}

function relog() {
	log.verbose("Renewing web session");

	var config = Config.get();
	var username = config.steam.last;

	client.oAuthLogin(config.steam.sentries[username], client.oAuthToken, function(err, sessionID, cookies) {
		if(err) {
			log.warn("Cannot renew web session: " + err.message);
			return;
		}

		log.verbose("Web session renewed");
		g_Cookies = cookies;
		require('./trade.js').setCookies(cookies);
	});
}

function checkOfferCount() {
	request.get({
		"uri": "https://api.steampowered.com/IEconService/GetTradeOffersSummary/v1/?key=" + require('./trade.js').apiKey,
		"json": true
	}, function(err, response, body) {
		if(err || response.statusCode != 200) {
			log.warn("Cannot get trade offer count: " + (err ? err.message : "HTTP error " + response.statusCode));
			return;
		}

		if(!body.response) {
			log.warn("Cannot get trade offer count: malformed response");
			return;
		}

		body = body.response;

		log.verbose(body.pending_received_count + " incoming offer" + (body.pending_received_count == 1 ? '' : 's') +
				" (" + body.escrow_received_count + " on hold), " +
				body.pending_sent_count + " sent offer" + (body.pending_sent_count == 1 ? '' : 's') +
				" (" + body.escrow_sent_count + " on hold)");
	});
}

function promptLogin() {
	var config = Config.get();

	Prompt.get({
		"properties": {
			"username": {
				"description": "Steam username".green,
				"type": "string",
				"required": true,
				"default": config.steam ? config.steam.last : null
			},
			"password": {
				"description": "Steam password".green + " (hidden)".red,
				"type": "string",
				"required": true,
				"hidden": true
			}
		}
	}, function(err, result) {
		if(err) {
			log.error("Cannot read Steam details: " + err.message);
			process.exit(1);
			return;
		}

		var username = result.username.toLowerCase();
		var password = result.password;
		var sentry;

		if (config.steam && config.steam.sentries) {
			sentry = config.steam.sentries[username];
		}

		config.steam = config.steam || {};
		config.steam.last = username;
		Config.write(config);

		performLogin();

		function performLogin(details) {
			details = details || {"accountName": username, "password": password, "steamguard": sentry};

			client.login(details, function(err, sessionID, cookies, steamguard, oAuthToken) {
				if(err) {
					// There was a problem logging in
					switch(err.message) {
						case 'SteamGuard':
						case 'SteamGuardMobile':
							// Steam Guard denied this login
							Prompt.get({
								"properties": {
									"authCode": {
										"description": ("Steam Guard" + (err.message == "SteamGuardMobile" ? " app" : "") + " code").green,
										"type": "string",
										"required": true
									}
								}
							}, function(err2, result) {
								if(err2) {
									log.error("Cannot read auth code: " + err2.message);
									process.exit(1);
									return;
								}

								details[err.message == "SteamGuardMobile" ? "twoFactorCode" : "authCode"] = result.authCode;
								performLogin(details);
							});
							break;

						case 'CAPTCHA':
							// We couldn't login because we need to fill in a captcha
							open(err.captchaurl);
							Prompt.get({
								"properties": {
									"captcha": {
										"description": "CAPTCHA".green,
										"type": "string",
										"required": true
									}
								}
							}, function(err, result) {
								if(err) {
									log.error("Cannot read CAPTCHA: " + err.message);
									process.exit(1);
									return;
								}

								details.captcha = result.captcha;
								performLogin(details);
							});
							break;

						default:
							// Some other error occurred
							log.error("Login failed: " + err.message);
							promptLogin();
					}
				} else {
					// Logged in successfully, ask the user if they want to remember this login
					config.steam.sentries[username] = steamguard;
					Prompt.confirm("Remember login?".green, {"default": "no"}, function(err, save) {
						save = err ? false : save;

						if(save) {
							delete config.steam.cookies;
							config.steam.oAuthTokens = config.steam.oAuthTokens || {};
							config.steam.oAuthTokens[username] = oAuthToken;
							Config.write(config);
						}

						g_Cookies = cookies;
						log.info("Logged into Steam!");

						checkLogin();
					});

					var sid = client.steamID.getSteamID64();
					if(config.steam.identitySecret && config.steam.identitySecret[sid]) {
						client.startConfirmationChecker(10000, config.steam.identitySecret[sid]);
					}
				}
			});
		}
	});
}

function promptFamilyView() {
	Prompt.get({
		"properties": {
			"pin": {
				"description": "Family View PIN (hidden)",
				"type": "string",
				"required": true,
				"hidden": true
			}
		}
	}, function(err, result) {
		if(err) {
			log.error("Cannot read PIN: " + err.message);
			promptFamilyView();
		} else {
			client.parentalUnlock(result.pin, function(err) {
				if(err) {
					log.error("Unlock failed: " + err.message);
					promptFamilyView();
				} else {
					checkLogin();
				}
			})
		}
	})
}
