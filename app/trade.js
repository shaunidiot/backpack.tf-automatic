var TradeOfferManager = require('steam-tradeoffer-manager');
var Config = require('./config.js');
var log = require('./logging.js');
var fs = require('fs');

const POLLDATA_FILENAME = 'polldata.json';

var manager = new TradeOfferManager({
	"language": "en",
	"pollInterval": 10000
});

if(fs.existsSync(POLLDATA_FILENAME)) {
	try {
		manager.pollData = JSON.parse(fs.readFileSync(POLLDATA_FILENAME));
	} catch(e) {
		log.verbose("polldata.json is corrupt");
	}
}

manager.on('pollData', function(pollData) {
	fs.writeFile(POLLDATA_FILENAME, JSON.stringify(pollData));
});

manager.on('newOffer', function(offer) {
	log.info(offer.partner.getSteam3RenderedID() + " Incoming offer #" + offer.id + " from " + offer.partner.toString());

	if(offer.itemsToGive.length === 0 || offer.itemsToReceive.length === 0) {
		// Gift offer
		log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " is a gift offer, skipping");
		return;
	}

	var currencies = {
		"metal": 0,
		"keys": 0
	};

	var config = Config.get();

	// TODO: Buy orders
	var item;
	for(var i = 0; i < offer.itemsToReceive.length; i++) {
		item = offer.itemsToReceive[i];

		if(item.appid != 440) {
			log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " contains non-TF2 items, skipping");
			return;
		}

		var metalValue = getMetalValue(item);

		if(metalValue > 0) {
			currencies.metal += metalValue;
		} else if(config.acceptedKeys && config.acceptedKeys.indexOf(item.market_hash_name) != -1) {
			currencies.keys++;
		} else if(!config.acceptGifts) {
			log.info(offer.partner.getSteam3RenderedID() + " Offer #" + offer.id + " contains non-currency item " + item.name + ", skipping");
			return;
		}
	}

	// Fix x.99999 metal values
	if(currencies.metal % 1 >= 0.99) {
		currencies.metal = Math.round(currencies.metal);
	}

	require('./backpacktf.js').handleOffer(offer, currencies);
});

function isWeapon(item) {
	var type = item.getTag('Type');
	if(!type) {
		return false;
	}

	if(item.market_hash_name.match(/(Class|Slot) Token/)) {
		return false;
	}

	// Find out if it's uncraftable
	var uncraftable = (item.descriptions || []).some(function(description) {
		return description.value && description.value.match(/Usable in Crafting/i);
	});

	if(uncraftable) {
		return false;
	}

	return ['Primary weapon', 'Secondary weapon', 'Melee weapon', 'Primary PDA', 'Secondary PDA'].indexOf(type.name) != -1;
}

function getMetalValue(item) {
	if(isWeapon(item)) {
		return 1/18;
	}

	switch(item.market_hash_name) {
		case "Scrap Metal":
			return 1/9;

		case "Reclaimed Metal":
			return 1/3;

		case "Refined Metal":
			return 1;
	}

	return 0;
}

manager.on('receivedOfferChanged', function(offer, oldState) {
	log.verbose("Offer #" + offer.id + " changed: " + TradeOfferManager.getStateName(oldState) + " -> " + TradeOfferManager.getStateName(offer.state));

	if(offer.state == TradeOfferManager.ETradeOfferState.InvalidItems) {
		log.info("Offer #" + offer.id + " is now invalid, declining");
		offer.decline();
	}
});

module.exports = manager;
