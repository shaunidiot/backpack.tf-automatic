var Winston = require('winston');
var moment = require('moment');

const LOG_LEVELS = {
	"debug": 0,
	"verbose": 1,
	"info": 2,
	"warn": 3,
	"error": 4,
	"trade": 5
};

const LOG_COLORS = {
	"debug": "blue",
	"verbose": "cyan",
	"info": "green",
	"warn": "yellow",
	"error": "red",
	"trade": "magenta"
};

// Instantiate basic logger since we don't have config yet
var logger = new Winston.Logger({
	"levels": LOG_LEVELS,
	"colors": LOG_COLORS
});

// Export it
module.exports = logger;

// Now let's get our config
var config = require('./config.js').get();

// Set up a new console logger
logger.add(Winston.transports.Console, {
	"name": "console",
	"level": (config.logs && config.logs.console && config.logs.console.level) ? config.logs.console.level : "info",
	"colorize": true,
	"timestamp": getTimestamp
});

// See if we want a log file, and if so, add it
if (config.logs && config.logs.file && !config.logs.file.disabled) {
	logger.add(Winston.transports.File, {
		"name": "log.all",
		"level": config.logs.file.level || "warn",
		"filename": "automatic.log",
		"json": false,
		"timestamp": getTimestamp
	});
}

// See if we want a trade log file, and if so, add it
if (config.logs && config.logs.trade && !config.logs.trade.disabled) {
	logger.add(Winston.transports.File, {
		"name": "log.trade",
		"level": "trade",
		"filename": "automatic.trade.log",
		"json": false,
		"timestamp": getTimestamp
	});
}

// This function returns our timestamp format
function getTimestamp() {
	var steam = require('./steamclient.js');
	return (steam.username ? '[' + steam.username + '] ' : '') + moment().format(config.dateFormat || "HH:mm:ss");
}
